package sixgreen.ddl;

public interface Capabilities {

	String getDescription();

	boolean isUsingDefaultValues();

	Float getTextScaleMultiplier();

	/*
	 * Since Android 2.3 (�Gingerbread�) this is available via
	 * android.os.Build.SERIAL. Devices without telephony are required to report
	 * a unique device ID here; some phones may do so also.
	 */
	boolean hasValidBuildSerial();

	boolean isTablet();

	boolean allowExtra(String key);
}
