package sixgreen.ddl;

import android.util.Log;

import java.util.Locale;

public class DeviceCheck {


	public static void loginfo(String tag) {
		Locale locale = CapabilityLoader.getDeviceLocale();
		Log.d(tag, "============= Device Defect Library ==============");
		Log.d(tag, "Device Identity: " + locale.toString());
		Log.d(tag, "==================================================");
	}

	public static String getDescription() {
		return CapabilityLoader.getCapabilities().getDescription();
	}

	///**
	// * @param tag
	// * @deprecated call DeviceCheck.loginfo(String tag) instead.
	// */
	//@Deprecated
	//public void dump(String tag) {
	//	Locale locale = CapabilityLoader.getDeviceLocale();
	//	Log.d(tag, "FIXME  - SHOULD CALL DeviceCheck.loginfo(String tag) INSTEAD.");
	//	DeviceCheck.loginfo(tag);
	//}

	public static boolean allowExtra(String key) {
		if (!CapabilityLoader.getCapabilities().isUsingDefaultValues()) {
			return CapabilityLoader.getCapabilities().allowExtra(key);
		}

		// default to true.
		return true;
	}

	/**
	 * Is this device known to have incorrectly implemented or badly behaved code.
	 *
	 * @return true is this device is likley to need this library.
	 */
	public static boolean hasDefects() {
		return !CapabilityLoader.getCapabilities().isUsingDefaultValues();
	}

	// private final DeviceType deviceType;

	// public DeviceCheck() {
	// deviceType = DeviceType.detect();
	// }

	public static int scaleTextSize(int size) {
		int newSize = size;

		if (!CapabilityLoader.getCapabilities().isUsingDefaultValues()) {
			newSize = (int) (CapabilityLoader.getCapabilities()
											 .getTextScaleMultiplier() * size);
		}

		Log.d("DeviceCheck", "Font size was [" + size + "], scaling to ["
							 + newSize + "]");
		return newSize;
	}
}
