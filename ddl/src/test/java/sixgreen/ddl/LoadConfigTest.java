package sixgreen.ddl;

import android.content.Context;
import android.util.Log;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Locale;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
@RunWith(MockitoJUnitRunner.class)
//@PrepareForTest(android.util.Log.class)
public class LoadConfigTest {

	private static final String EXTRA1 = "my.fake.extra.property1";
	private static final String EXTRA2 = "my.fake.extra.property2";
	private static final String EXTRA3 = "my.fake.extra.property3";
	private static final String NO_MANUFACTURER = "nosuchma";
	private static final String NO_DEVICE = "nosuchde";
	private static final String NO_MODEL = "nosuchmo";
	private static final String FAKE_MANUFACTURER = "test";
	private static final String FAKE_DEVICE = "FAKE";
	private static final String FAKE_MODEL = "BOGUS";
	private static final String BLANK_STRING = "";
	@Mock
	Context mMockContext;

	@Before
	public void onStartup() {
		//CapabilityLoader.setDeviceLocale(new Locale("test","FAKE","BOGUS"));

		//when(mMockContext.getString(R.string.hello_word))
		//		.thenReturn(FAKE_STRING);

		Mockito.mock(Log.class);

		//mock(Log.class, new Answer() {
		//	@Override
		//	public Object answer(InvocationOnMock invocation) throws Throwable {
		//		return null;
		//	}
		//})
	}

	@Test
	public void testDefaultValues() throws Exception {
		CapabilityLoader.setDeviceLocale(new Locale(NO_MANUFACTURER, NO_DEVICE, NO_MODEL));


		assertEquals("Unhandled Device", DeviceCheck.getDescription());

		assertFalse("Default config should not have any faults to check", DeviceCheck
				.hasDefects());

		assertFalse(DeviceCheck.allowExtra(EXTRA1));
		assertTrue(DeviceCheck.allowExtra(EXTRA2));
		assertTrue(DeviceCheck.allowExtra(EXTRA3));
	}

	@Test
	public void testManufacturerValuesUnsetSecondaries() throws Exception {
		CapabilityLoader.setDeviceLocale(new Locale(FAKE_MANUFACTURER));

		assertEquals("Unit Test Manufacturer", DeviceCheck.getDescription());

		assertTrue(DeviceCheck.hasDefects());

		assertFalse(DeviceCheck.allowExtra(EXTRA1));
		assertTrue(DeviceCheck.allowExtra(EXTRA2));
		assertTrue(DeviceCheck.allowExtra(EXTRA3));
	}

	@Test
	public void testManufacturerValuesEmptySecondaries() throws Exception {
		CapabilityLoader.setDeviceLocale(new Locale(FAKE_MANUFACTURER, BLANK_STRING, BLANK_STRING));

		assertEquals("Unit Test Manufacturer", DeviceCheck.getDescription());

		assertTrue(DeviceCheck.hasDefects());

		assertFalse(DeviceCheck.allowExtra(EXTRA1));
		assertTrue("Expected extra to be allowed", DeviceCheck
				.allowExtra(EXTRA2));
		assertTrue(DeviceCheck.allowExtra(EXTRA3));
	}

	@Test
	public void testDeviceValuesNullSecondaries() throws Exception {
		CapabilityLoader.setDeviceLocale(new Locale(FAKE_MANUFACTURER, FAKE_DEVICE));

		assertEquals("Unit Testing Device", DeviceCheck.getDescription());

		assertTrue(DeviceCheck.hasDefects());
		assertTrue(DeviceCheck.allowExtra(EXTRA1));
		assertTrue(DeviceCheck.allowExtra(EXTRA2));
		assertFalse(DeviceCheck.allowExtra(EXTRA3));
	}

	@Test
	public void testModelValuesNullSecondaries() throws Exception {
		CapabilityLoader.setDeviceLocale(new Locale(FAKE_MANUFACTURER, FAKE_DEVICE, FAKE_MODEL));

		assertEquals("Unit Testing Model", DeviceCheck.getDescription());

		assertTrue(DeviceCheck.hasDefects());
		assertTrue(DeviceCheck.allowExtra(EXTRA1));
		assertTrue(DeviceCheck.allowExtra(EXTRA2));
		assertFalse(DeviceCheck.allowExtra(EXTRA3));
	}
}